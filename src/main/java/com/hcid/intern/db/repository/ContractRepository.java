package com.hcid.intern.db.repository;


import com.hcid.intern.db.model.ContractData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository
public interface ContractRepository extends JpaRepository<ContractData, String> {
}
