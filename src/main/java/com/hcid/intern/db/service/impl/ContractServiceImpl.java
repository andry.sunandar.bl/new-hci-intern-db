package com.hcid.intern.db.service.impl;


import com.hcid.intern.db.model.ContractData;
import com.hcid.intern.db.repository.ContractRepository;
import com.hcid.intern.db.service.ContractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ContractServiceImpl implements ContractService {

    @Autowired
    private ContractRepository contractRepository;



    public void saveContract(ContractData contractData){
        contractRepository.save(contractData);
    }

    @Override
    public void updateContract(ContractData contractData) {
        contractRepository.save(contractData);
    }

    @Override
    public void deleteContract(String contractId) {
        contractRepository.delete(contractId);
    }


}
