package com.hcid.intern.db.service;


import com.hcid.intern.db.model.ContractData;

public interface ContractService {

    void saveContract(ContractData contractData);

    void updateContract(ContractData contractData);

    void deleteContract(String contractId);
}
