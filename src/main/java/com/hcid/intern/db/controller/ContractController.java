package com.hcid.intern.db.controller;



import com.hcid.intern.db.model.ContractData;
import com.hcid.intern.db.service.ContractService;
import com.hcid.intern.db.service.impl.ContractServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api/v1/contract")
public class ContractController {

    @Autowired
    private ContractService contractService;

    @PostMapping("/save-contract")
    public ResponseEntity<?> saveContract(@RequestBody ContractData contractData){

        // do validation

        ContractData contractData1 = new ContractData();
        contractData1.setName("jonny kemot");
        contractData1.setContractNumber("24234234");

        // save contract data
        contractService.saveContract(contractData);

        return new ResponseEntity(contractData1, HttpStatus.OK);
    }

    @PostMapping("/update-contract")
    public ResponseEntity<?> updateContract(@RequestBody ContractData contractData){

        // do validation

        // save contract data
        contractService.updateContract(contractData);

        return new ResponseEntity(contractData, HttpStatus.OK);
    }

    @GetMapping("/delete-contract/{contractId}")
    public ResponseEntity<?> deleteContract(@PathVariable String contractId){

        // do validation

        // save contract data
        contractService.deleteContract(contractId);

        return new ResponseEntity(contractId, HttpStatus.OK);
    }
}
